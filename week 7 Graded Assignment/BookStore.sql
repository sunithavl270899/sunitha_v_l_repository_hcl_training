create database hello;
use hello;

create table login(username varchar(10) not null,password varchar(10) not null);
insert into login values("sunitha",1234);
insert into login values("mahesh",123);
select * from login;

create table registration(username varchar(10) not null,password varchar(10) not null,email varchar(40));
insert into registration values("sunitha",1234,'sunithavl270899@@gmail.com');
insert into registration values("mahesh",123,'mahesh541@gmail.com');

select * from registration;

create table books(bookid int primary key,booktitle varchar(40),bookgenre varchar(20),image varchar(40));
insert into books values(1," Algorithm of data and Architecture","Motivational","Algorithm of data and Architecture.jpg");
insert into books values(2,"Compiler Design","Motivational","Compiler Design.jpg");
insert into books values(3,"Fundamental Database System"," Motivational","Fundamental Database System.jpg");
insert into books values(4,"Microprocessor","Motivational","Microprocessor.jpg");
insert into books values(5," Motivation book","Motivational"," Motivation book.jpg");
insert into books values(6,"Quore and Core of Java"," Motivational"," Quore and Core of Java.jpg");

select * from books;