<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
<style >
body{
margin: 0;
padding: 0;
text-align: center;
background-color: pink;
}
</style>

</head>
<body>
<div class="container">
		<div style="text-align:center;font-size:40px"> All the Books Available in online shoping</div>
		<div class="row">
	<a href="logout.jsp"><button> Logout</button></a>	
		<style>
		.sunitha{
		float:left;
		background-color: lightgrey;
		height:520px;
  		width: 300px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Lakshmi{
		float:left;
		background-color: lightgrey;
		height:520px;
  		width: 300px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Anitha{
		float:left;
		background-color: lightgrey;
		height:520px;
  		width: 300px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Subhadra{
		float:left;
		background-color: lightgrey;
		height:520px;
  		width: 300px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.DurgaRao{
		float:left;
		background-color: lightgrey;
		height:520px;
  		width: 300px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Shine{
		float:left;
		background-color: lightgrey;
		height:520px;
  		width: 300px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
</style>
		
		<div class="sunitha">
		<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="BookImage" src="image/Algorithm of data and Architecture.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:1</h5>
						<h5 class="booktitle" >BookTitle:"Algorithm of data and Architecture"</h5>
						<h6 class="bookgenre">BookGenre:"Motivational"</h6>
						<h6 class="bookPublisher"> BookPublisher:"Random House Trade Paperbacks"</h6>
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="like.jsp"><button>Like Book</button></a> 	
							<a href="readlater.jsp"><button>ReadLater Books</button></a>					</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="Lakshmi">
		
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/Compiler Design.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:2</h5>
						<h5 class="booktitle" >BookTitle:"Compiler Design"</h5>
						<h6 class="bookgenre">BookGenre:"Motivational"</h6>
						<h6 class="bookPublisher"> BookPublisher:"HarperCollins"</h6>
						<div class="mt-3 d-flex justify-content-between"><br>
							<a class="btn btn-dark" href="like.jsp"><button>Like Book</button></a> 
							<a href="readlater.jsp"><button>ReadLater Books</button></a>
						</div>
					</div>
				</div>	
			</div>	
		</div>	
			
			<div class="Anitha">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/Fundamental Database System.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:3</h5>
						<h5 class="booktitle" >BookTitle:"Fundamental Database System"</h5>
						<h6 class="bookgenre">Bookgenre:"Motivational"</h6>
						<h6 class="bookPublisher"> BookPublisher:"JAICO BOOKS"</h6>
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="like.jsp"><button>Like Book</button></a> 
							<a href="readlater.jsp"><button>ReadLater Books</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Subhadra">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/Microprocessor.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:4</h5>
						<h5 class="booktitle" >BookTitle:"Microprocessor"</h5>
						<h6 class="bookgenre">Bookgenre:"Motivational"</h6>
						<h6 class="bookPublisher"> BookPublisher:"Simon & Schuster"</h6>
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="like.jsp"><button>Like Book</button></a> 
							<a href="readlater.jsp"><button>ReadLater Books</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="DurgaRao">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="BookImage" src="image/Motivation book.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:5</h5>
						<h5 class="booktitle" >BookTitle:"Motivation book"</h5>
						<h6 class="bookgenre">BookGenre:"Motivational""</h6>
						<h6 class="bookPublisher"> BookPublisher:"John Murray Press"</h6>
						<div class="mt-3 d-flex justify-content-between"><br>
							<a class="btn btn-dark" href="like.jsp"><button>Like Book</button></a>
							<a href="readlater.jsp"><button>ReadLater Books</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Shine">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/Quore and Core of Java.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:6</h5>
						<h5 class="booktitle" >BookTitle:"Quore and Core of Java"</h5>
						<h6 class="bookgenre">BookGenre:"Motivational"</h6>
						<h6 class="bookPublisher"> BookPublisher:"Simon & Schuster"</h6>
						<div class="mt-3 d-flex justify-content-between"><br>
							<a class="btn btn-dark" href="like.jsp"><button>Like Book</button></a> 
							<a href="readlater.jsp"><button>ReadLater Books</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
			
		</div>
	</div>
			
</body>
</html>
