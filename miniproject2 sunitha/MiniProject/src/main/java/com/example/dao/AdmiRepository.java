package com.example.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.beans.Admin;

public interface AdmiRepository extends JpaRepository<Admin, String> {

}
