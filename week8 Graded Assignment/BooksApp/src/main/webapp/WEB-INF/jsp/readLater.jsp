<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Read Later</title>
<style>
body{
margin: 0;
padding: 0;
text-align: center;

}
</style>
</head>
<body>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<h1 style="font-size: 80px;color:#A95c68;text-align:center;">Books Store</h1>
	<h2 style="font-size: 80px;color:red;text-align:center;">Read Later Books</h2>
	
	<%
	String username = (String) session.getAttribute("username");
	if (username == null)
		username = "";
	out.print("<h4>User Name :  " + username + " </h4><br/ >");
	%>
	
	
	<table border="2" width="70%" cellpadding = "2" align ="center"style="background-color:#52ffff;font-size:25px;border:2px solid Violet">
		<tr>
			<th><b>BookId</b></th>
			<th><b>Name</b></th>
			<th><b>Author</b></th>
			<th><b>Image</b></th>		
		</tr>
		<c:forEach var="i" items="${books}">
			<tr align = "center">
					<td><c:out value="${i.getId() }"></c:out></td>
					<td><c:out value="${i.getName() }"></c:out></td>
					<td><c:out value="${i.getAuthor() }"></c:out></td>
					<td><img src=<c:out value="${i.getUrl() }"></c:out>width="180" height="175"></td>
   					<td><a href="readLaterDelete/${i.getId()}">Remove</a></td>  			
			</tr>
		</c:forEach>
	</table>
	<br/><br/>
	<input type='button' value='Logout'
			onclick="location.href= 'login'">
</body>
</html>