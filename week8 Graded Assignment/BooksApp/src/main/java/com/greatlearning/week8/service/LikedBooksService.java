package com.greatlearning.week8.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import com.greatlearning.week8.bean.*;
import com.greatlearning.week8.dao.*;

public class LikedBooksService {

	@Autowired
	LikedDAO dao;

	public List<LikedBooks> getAllBooks() {
		return dao.getAllBooks();
	}

	public boolean addBooks(int id, String name, String author, String url) throws DuplicateKeyException {
		return dao.addBooks(id, name, author, url);
	}

	public boolean deleteBook(int id) {
		return dao.deleteBook(id);
	}
	
	
}
