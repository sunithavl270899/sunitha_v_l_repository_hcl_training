create database bookstoresunitha;
use bookstoresunitha;

create table BookUsers(
username varchar(30) primary key  ,
password varchar(30)
);

create table Books(
id int primary key,
name varchar(60),
author varchar(60),
url varchar(10000)
);

insert into books values(1, "The Harrey potter", "Clayton Chrisrensen","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Harry-Potter-and-the-Goblet-of-Fire-book-cover.jpg?resize=500,762&quality=82&strip=all");
insert into books values(2, "The Ben-Hur-A-Tale-of-the-Christ",  "Tracy Kidder","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Ben-Hur-A-Tale-of-the-Christ-book-cover.jpg?resize=500,750&quality=82&strip=all");
insert into books values(3, "Lolita-Book-Cover-3",  "Richard","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Lolita-Book-Cover-3.jpg?resize=500,838&quality=82&strip=all");
insert into books values(4, "Heidi", "Thomas Friedman","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Heidi-book-cover.jpg?resize=500,545&quality=82&strip=all");
insert into books values(5, "Anne-of-Green-Gables-book-cover", "Thomas Friedman","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Anne-of-Green-Gables-book-cover.jpg?resize=500,700&quality=82&strip=all");
insert into books values(6, "Black-Beauty", "TR Reid","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Black-Beauty.jpg?resize=500,719&quality=82&strip=all");
insert into books values(7, "The-Name-of-the-Rose", "Richard",  "https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/The-Name-of-the-Rose.jpg?resize=500,761&quality=82&strip=all");
insert into books values(8, "The-Eagle-Has-Landed-book-cover", "George Gilder","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/The-Eagle-Has-Landed-book-cover.jpg?resize=500,767&quality=82&strip=all");
insert into books values(9, "Watership-Down-book-cover","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Watership-Down-book-cover.jpg?resize=500,762&quality=82&strip=all");

create table ReadLater(
id int primary key,
name varchar(60),
author varchar(60),
url varchar(10000)
);


create table Likelist(
id int primary key,
name varchar(60),
author varchar(60),
url varchar(10000)
);