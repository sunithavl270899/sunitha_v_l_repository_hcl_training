package com.gl.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name="books")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Books {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String name;
	private String genre;
	

	public Books(int i, String string, String string2, Object object) {
		// TODO Auto-generated constructor stub
	}


	public Books() {
		// TODO Auto-generated constructor stub
	}


	public int getId() {
		return id;
	}


	protected void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	protected void setName(String name) {
		this.name = name;
	}


	public String getGenre() {
		return genre;
	}


	protected void setGenre(String genre) {
		this.genre = genre;
	}


	protected List<User> getUsers() {
		return users;
	}


	protected void setUsers(List<User> users) {
		this.users = users;
	}


	@JsonIgnore
	@ManyToMany(targetEntity = User.class, mappedBy = "likedBooks", cascade = {CascadeType.PERSIST, CascadeType.DETACH,CascadeType.MERGE,CascadeType.REFRESH})
	private List<User> users;
}
